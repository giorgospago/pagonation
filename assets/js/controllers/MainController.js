app.filter('offset', function(){
	return function(input, start){
		start = parseInt(start, 10);
		return input.slice(start);
	};
});

app.directive('errSrc', function() {
    return {
        link: function(scope, element, attrs) {
            if(!attrs.src)
                attrs.$set('src', attrs.errSrc);

            element.bind('error', function () {
                if (attrs.src != attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
});

app.controller('MainController', function($rootScope,$scope, $http){
	
	$scope.items = [];

	$http.get('list.json').then(r => $scope.items = r.data);

    $scope.currentPage = 0;
    $scope.quantity = 6;
    $scope.sort = 'id';
	
	// PAGINATION
	$scope.range = function(){
		var rsiz = Math.ceil($scope.filtered.length/$scope.quantity);
		var rangeSize = (rsiz > 8)?8:rsiz;
		var ret = [];
		var start;

		start = $scope.currentPage;
		if ( start > $scope.pageCount()-rangeSize ){
		  start = $scope.pageCount()-rangeSize+1;
		}

		for (var i=start; i<start+rangeSize; i++) {
		  ret.push(i);
		}
		return ret;
	};
	$scope.prevPage = function() {
		if($scope.currentPage > 0) {
			$scope.currentPage--;
		}
	};
	$scope.prevPageDisabled = function() {
		return $scope.currentPage === 0 ? "disabled" : "";
	};
	$scope.pageCount = function() {
		return Math.ceil($scope.filtered.length/$scope.quantity)-1;
	};
	$scope.nextPage = function() {
		if ($scope.currentPage < $scope.pageCount()) {
			$scope.currentPage++;
		}
	};
	$scope.nextPageDisabled = function() {
		return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	};
	$scope.setPage = function(n) {
		$scope.currentPage = n;
	};
	
});
